import cv2
import os
import numpy as np

# Set the path which includes target images
path = 'datasets/SNUH/20220104'
# Path folder must have no sub folders inside
file_list = os.listdir(path)


def multi_tiff_merge(img_file):
    tiff = cv2.imreadmulti(img_file, flags=cv2.IMREAD_ANYDEPTH)
    img = tiff[1]

    img_a = img[0]
    img_b = img[1]
    img_c = img[2]

    img_a_8bit = img_a // 16
    img_b_8bit = img_b // 16
    img_c_8bit = img_c // 16

    img_merge = cv2.merge([img_a_8bit, img_b_8bit, img_c_8bit])
    img_merge = img_merge.astype(np.uint8)
    output_file = os.path.splitext(img_file)[0] + '_merge.tif'

    cv2.imwrite(output_file, img_merge)



for i in file_list:
    img_file = os.path.join(path, i)
    multi_tiff_merge(img_file)

